/// Jump N Bump Menu
/// Copyright (C) 2003 2004 Martin Willemoes Hansen <mwh@sysrq.dk>
///
/// This program is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
/// jumpnbumpmenu.cs The entire program

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Collections;

using Mono.Unix;

using GLib;
using Gdk;
using Gtk;
using GtkSharp;
using Glade;
using Gnome;

class Level {
	public string Name;
	public string Location;

	public override string ToString()
	{
		return Name;
	}
}

class JumpnbumpMenu {
	string [] LEVELPATHS = new string [] {"/usr/share/jumpnbump", 
					      "/usr/share/games/jumpnbump",
					      "/usr/local/share/jumpnbump", 
					      Environment.GetFolderPath (
					      Environment.SpecialFolder.Personal) + 
					      Path.DirectorySeparatorChar +
					      "jumpnbump"};
	string selectedLevel;
	ListStore store;
	TreeView levels;

	[Glade.Widget] Gtk.Window main;
	[Glade.Widget] RadioButton standalone, fireworks, client, server;
	[Glade.Widget] ScrolledWindow sw;
	[Glade.Widget] Gtk.Image image;
	[Glade.Widget] SpinButton num_clients, player_num;
	[Glade.Widget] Gtk.Entry serverurl;
	[Glade.Widget] CheckButton fullscreen, nogore, double_res, mirror, nosound, noflies, withmusic;

	public JumpnbumpMenu() 
	{
		Catalog.Init ("jumpnbumpmenu", Locale.PATH);
		Glade.XML gui = new Glade.XML (null, "jumpnbumpmenu.glade", "main", "jumpnbumpmenu");
		gui.Autoconnect (this);
		main.Icon = new Pixbuf (null, "icon.jpeg");
		Idle.Add (new IdleHandler (LoadLevels));
		Idle.Add (new IdleHandler (SelectDefaultLevel));
	}

	bool SelectDefaultLevel()
	{
		TreeIter iter;
		levels.Model.GetIterFirst (out iter);

		do {
			if (levels.Model.GetValue (iter, 0).ToString() == "jumpbump") {
				levels.Selection.SelectIter (iter);
				LevelChanged (null, null);
				break;
			}
			
		} while (levels.Model.IterNext (ref iter));
		return false;
	}

	bool LoadLevels()
	{
		store = new ListStore (typeof (object));
		ArrayList files = new ArrayList();
		foreach (string path in LEVELPATHS) {
			DirectoryInfo dir_info = new DirectoryInfo (path);
			if (!dir_info.Exists)
				continue;
			
			files.AddRange (dir_info.GetFiles ("*.dat"));
		}

		TreeIter iter;
		foreach (FileInfo file in files) {
			iter = store.Append();
			Level level = new Level();
			level.Name = file.Name.Substring (0, file.Name.Length - 4);
			level.Location = file.DirectoryName + 
				Path.DirectorySeparatorChar + file.Name;
			store.SetValue (iter, 0, new GLib.Value (level));
		}

		levels = new TreeView (store);
		levels.CursorChanged += new EventHandler (LevelChanged);
		levels.AppendColumn (Catalog.GetString ("Level"), 
				     new CellRendererText (), 
				     new TreeCellDataFunc (CellData));

		sw.Add (levels);
		sw.ShowAll();
		return false;
	}

	void CellData (TreeViewColumn tree_column, CellRenderer cell, 
		       TreeModel tree_model, 
		       TreeIter iter)
	{
		Level level = (Level) store.GetValue (iter, 0);
		((CellRendererText) cell).Text = level.Name;
	}
		
	void Quit (object o, EventArgs e) 
	{
		Application.Quit();
	}

	void About (object o, EventArgs e)
	{
		Assembly assembly = Assembly.GetExecutingAssembly ();

		string [] authors = new String [] {
			"Martin Willemoes Hansen",
		};

		string [] documenters = new String [] {};

		// Translators should localize the following string
		// * which will give them credit in the About box.
		// * E.g. "Martin Willemoes Hansen <mwh@sysrq.dk>"
		string translators = Catalog.GetString ("translator-credits");

		Pixbuf pixbuf = new Pixbuf(null, "logo.jpeg");
			
		new Gnome.About ("Jump n Bump Menu", assembly.GetName().Version.ToString(),
				 Catalog.GetString (
@"Copyright (C) 2002 2003 2004 Martin Willemoes Hansen
Jump n Bump Menu comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to
redistribute it under certain conditions;
see the text file: LICENSE, distributed
with this program."),
				 Catalog.GetString ("Mono, Gtk# and Jump n Bump rock!"), 
				 authors, documenters, translators, pixbuf).Show();
	}

	void SetLevelSensitive (bool sensitive)
	{
		levels.Sensitive = sensitive;
		mirror.Sensitive = sensitive;
		nogore.Sensitive = sensitive;
		nogore.Active = false;
		noflies.Sensitive = sensitive;
		noflies.Active = false;
	}

	void SetRemoteServerSensitive (bool sensitive)
	{
		serverurl.Sensitive = sensitive;
		player_num.Sensitive = sensitive;
	}

	void StandaloneMode (object o, EventArgs args) 
	{
		if (!standalone.Active)
			return;

		SetLevelSensitive (true);
		SetRemoteServerSensitive (false);
		num_clients.Sensitive = false;
	}

	void FireworksMode (object o, EventArgs e) 
	{
		if (!fireworks.Active)
			return;

		SetLevelSensitive (false);
		SetRemoteServerSensitive (false);
		num_clients.Sensitive = false;
	}

	void ClientMode (object o, EventArgs e) 
	{
		if (!client.Active)
			return;

		SetLevelSensitive (true);
		SetRemoteServerSensitive (true);
		num_clients.Sensitive = false;
	}

	void ServerMode (object o, EventArgs e) 
	{
		if (!server.Active)
			return;

		SetLevelSensitive (true);
		SetRemoteServerSensitive (false);
		num_clients.Sensitive = true;
	}

	void LevelChanged (object o, EventArgs e) 
	{
		Idle.Add (new IdleHandler (LevelChanged));
	}

	bool LevelChanged() 
	{
		TreeModel model;
		TreeIter iter;
		levels.Selection.GetSelected (out model, out iter);
		selectedLevel  = ((Level) model.GetValue (iter, 0)).Location;

		Directory.SetCurrentDirectory (Path.GetTempPath());

		Process jnbunpack = new Process();
		jnbunpack.StartInfo.FileName = "jumpnbump-unpack";
		jnbunpack.StartInfo.Arguments = selectedLevel;
		jnbunpack.StartInfo.UseShellExecute = false;
		jnbunpack.StartInfo.RedirectStandardOutput = true;
		jnbunpack.Start();
		jnbunpack.WaitForExit();

		Process scale = new Process();
		scale.StartInfo.FileName = "convert";
		scale.StartInfo.Arguments = "-scale 50% level.pcx level_scaled.pcx";
		scale.StartInfo.UseShellExecute = false;
		scale.Start ();
		scale.WaitForExit();

		Process convert = new Process();
		convert.StartInfo.FileName = "convert";
		convert.StartInfo.Arguments = "level_scaled.pcx level.png";
		convert.StartInfo.UseShellExecute = false;
		convert.Start();
		convert.WaitForExit();

		image.FromFile = Path.GetTempPath() + 
			Path.DirectorySeparatorChar + "level.png";

		return false;
	}

	void Run (object o, EventArgs e) 
	{
		Idle.Add (new IdleHandler (Run));
	}

	bool Run() 
	{
		if (standalone.Active)
			Execute (String.Format ("-dat {0} {1}", 
						selectedLevel, 
						CommonOptions()));

		else if (fireworks.Active)
			Execute (String.Format ("-fireworks {0}", 
						CommonOptions()));
		else if (client.Active)
			Execute (String.Format ("-player {0} -connect {1} -dat {2} {3}", 
						player_num.ValueAsInt,
						serverurl.Text,
						selectedLevel, 
						CommonOptions()));
		else
			Execute (String.Format ("-server {0} -dat {1} {2}", 
						num_clients.ValueAsInt,
						selectedLevel, 
						CommonOptions()));

		return false;
	}

	void Execute (string arguments)
	{
		Process ps = new Process();
		ps.StartInfo.FileName = "jumpnbump";
		ps.StartInfo.Arguments = arguments;
		ps.StartInfo.UseShellExecute = false;
		ps.Start();
	}

	string CommonOptions()
	{
		string options = "";

		if (fullscreen.Active)
			options += "-fullscreen";
		if (nogore.Active)
			options += " -nogore";
		if (double_res.Active)
			options += " -scaleup";
		if (nosound.Active)
			options += " -nosound";
		if (noflies.Active)
			options += " -noflies";
		if (withmusic.Active)
			options += " -musicnosound";
		if (mirror.Active)
			options += " -mirror";

		return options;
	}

	static void Main() 
	{
		Application.Init();
		new JumpnbumpMenu();
		Application.Run();
	}
}

